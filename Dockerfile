
FROM python:3.6

ENV PYTHONUNBUFFERED 1

RUN apt-get update && apt-get install -y \
    vim

WORKDIR /app

COPY public/ ./
